package com.ch;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.ch.math.Matrix4f;
import com.ch.math.Vector3f;

/**
 * is an extension of the Camera Class and provides additional functionality for
 * handling 3D camera input. It has a constructor that takes fov, aspect, zNear, and
 * zFar parameters to set the perspective projection matrix. The class also has a
 * method adjustToViewport which sets the viewport dimensions and calculates the view
 * matrix. Additionally, it has a processInput method that handles keyboard and mouse
 * input and moves the camera accordingly.
 */
public class Camera3D extends Camera {
	public Camera3D(float fov, float aspect, float zNear, float zFar) {
		super(new Matrix4f());
		this.values = new CameraStruct3D(fov, aspect, zNear, zFar);
		calculateProjectionMatrix(values);
	}

	/**
	 * Takes a `CameraStruct` object as input and returns a `Matrix4f` representation of
	 * the camera's projection matrix.
	 * 
	 * @param data 3D camera data for which the projection matrix is to be calculated.
	 * 
	 * @returns a `Matrix4f` object containing the camera's projection matrix.
	 */
	@Override
	public Matrix4f calculateProjectionMatrix(CameraStruct data) {
		return (projection = data.getAsMatrix4());
	}

	/**
	 * Adjusts the camera's aspect ratio to match the viewport size and updates the
	 * projection and view matrices for correct rendering within the viewport.
	 * 
	 * @param width horizontal dimension of the viewport in pixels.
	 * 
	 * @param height 2D viewport size of the camera's field of view and is used to calculate
	 * the camera's projection matrix.
	 */
	@Override
	public void adjustToViewport(int width, int height) {
		((CameraStruct3D) this.values).aspect = (float) width / height;
		calculateProjectionMatrix(values);
		try {
			calculateViewMatrix();
		} catch (NullPointerException e) {
		}
		GL11.glViewport(0, 0, width, height);
	}

	/**
	 * is a subclass of the CameraStruct class and provides additional functionality for
	 * handling 3D camera movements. It has four fields: fov, aspect, zNear, and zFar,
	 * which determine the perspective projection matrix. The class also includes a method
	 * for getting the perspective projection matrix as a Matrix4f object.
	 */
	protected class CameraStruct3D extends CameraStruct {

		public float fov, aspect, zNear, zFar;

		public CameraStruct3D(float fov, float aspect, float zNear, float zFar) {
			this.fov = fov;
			this.aspect = aspect;
			this.zNear = zNear;
			this.zFar = zFar;
		}

		/**
		 * initializes a `Matrix4f` object with a perspective projection matrix, given the
		 * field of view (fov), aspect ratio, near and far distances.
		 * 
		 * @returns a 4x4 matrix representation of a perspective projection transformation.
		 * 
		 * The function returns an instance of the `Matrix4f` class, which represents a 4x4
		 * matrix with floating-point elements. The matrix is initialized using the
		 * `initPerspective` method, which sets the values of the matrix based on the provided
		 * fov, aspect, zNear, and zFar parameters.
		 * 
		 * The `fov` parameter represents the field of view in radians, which determines the
		 * range of values that the matrix can handle. The `aspect` parameter represents the
		 * aspect ratio of the matrix, which is used to adjust the values of the matrix based
		 * on the aspect ratio of the input image. The `zNear` and `zFar` parameters represent
		 * the near and far clipping planes of the matrix, respectively, which determine the
		 * range of values that are considered "near" or "far" in the matrix.
		 * 
		 * Overall, the returned matrix represents a perspective projection matrix that can
		 * be used to transform points from a 3D space into a 2D image, based on the specified
		 * fov, aspect, zNear, and zFar values.
		 */
		public Matrix4f getAsMatrix4() {
			return new Matrix4f().initPerspective(fov, aspect, zNear, zFar);
		}

	}

	/**
	 * processes input events from the mouse and keyboard, updating the position and
	 * rotation of an object based on user inputs.
	 * 
	 * @param dt time step, which is used to calculate the movement of the object based
	 * on its speed and sensitivity.
	 * 
	 * @param speed 3D movement speed of the object being controlled, and its value is
	 * multiplied by the time elapsed (represented by `dt`) to determine the distance
	 * traveled during the frame.
	 * 
	 * @param sens sensitivity of the character's movement in response to mouse input,
	 * which determines how much the character will move based on the amount of mouse movement.
	 */
	public void processInput(float dt, float speed, float sens) {

		float dx = Mouse.getDX();
		float dy = Mouse.getDY();
		float roty = (float)Math.toRadians(dx * sens);
		getTransform().rotate(new Vector3f(0, 1, 0), (float) roty);
		getTransform().rotate(getTransform().getRot().getRight(), (float) -Math.toRadians(dy * sens));
		
		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
			speed *= 10;
		
		float movAmt = speed * dt;

		if (Keyboard.isKeyDown(Keyboard.KEY_W))
			move(getTransform().getRot().getForward(), movAmt);
		if (Keyboard.isKeyDown(Keyboard.KEY_S))
			move(getTransform().getRot().getForward(), -movAmt);
		if (Keyboard.isKeyDown(Keyboard.KEY_A))
			move(getTransform().getRot().getLeft(), movAmt);
		if (Keyboard.isKeyDown(Keyboard.KEY_D))
			move(getTransform().getRot().getRight(), movAmt);
		
	}

	/**
	 * updates the position of an object by adding a specified amount to its current
	 * position along a given direction.
	 * 
	 * @param dir 3D direction of movement, specifying how much the object's position
	 * should be moved in that direction.
	 * 
	 * 	- `dir`: A `Vector3f` object representing a 3D vector with x, y, and z components.
	 * 	- `amt`: An integer value representing the amount to move along the direction of
	 * `dir`.
	 * 
	 * @param amt amount of movement along the specified direction, which is added to the
	 * current position of the transform.
	 */
	private void move(Vector3f dir, float amt) {
		getTransform().setPos(getTransform().getPos().add(dir.mul(amt)));
	}

}
