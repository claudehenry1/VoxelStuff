package com.ch;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/**
 * in this file is used to represent a 3D model in a graphics engine. It contains a
 * vertex array object (VAO) and an index buffer object (IBO), which are used to store
 * the model's vertices and indices, respectively. The class provides methods for
 * binding and unbinding the VAO and IBO, as well as drawing the model and
 * enabling/disabling vertex attributes. Additionally, the class provides a method
 * for loading data from an array of vertices and an array of indices, which are used
 * to create the model.
 */
public class Model {
	private int vao, size;
	
	public Model(int vao, int count) {
		this.vao = vao;
		this.size = count;
	}
	
	/**
	 * binds a vertex array object, enables vertex attributes for position and texture
	 * coordinates, draws a collection of triangles using `GL_TRIANGLES`, disables the
	 * vertex attributes, and then unbinds the vertex array object.
	 */
	public void draw() {
		GL30.glBindVertexArray(vao);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		//GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, size);
		GL11.glDrawElements(GL11.GL_TRIANGLES, size, GL11.GL_UNSIGNED_INT, 0);
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL30.glBindVertexArray(0);
	}
	
	/**
	 * enables vertex attributes 0 and 1 for rendering in the GPU.
	 */
	public static void enableAttribs() {
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
	}
	
	/**
	 * disables vertex attributes 0 and 1 of an OpenGL 2D context.
	 */
	public static void disableAttribs() {
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
	}
	
	/**
	 * retrieves the value of a variable 'vao'.
	 * 
	 * @returns an integer value representing the `vao` field.
	 */
	public int getVAO() {
		return vao;
	}
	
	/**
	 * returns the value of a field named `size`.
	 * 
	 * @returns the value of the `size` field.
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * loads a 3D model into memory by creating a Vertex Array Object (VAO), storing index
	 * data, and then binding the VAO to the graphics pipeline.
	 * 
	 * @param vertices 3D model's geometry data as a float array, which is stored in the
	 * Vertex Array Object (VAO) for rendering purposes.
	 * 
	 * 	- `float[] vertices`: An array of `float` values representing 3D coordinates for
	 * model geometry.
	 * 	- `int[] indices`: An array of integers representing the vertex indices for drawing
	 * the model in a particular order.
	 * 
	 * The function first creates an Vulkan Vertex Array Object (VAO) using the `createVAO()`
	 * method, which is then used to store the indices and vertex data. The indices are
	 * stored using the `storeIndices()` method, followed by the vertex data using the
	 * `storeData()` method. Finally, the VAO is unbound using the `unbindVAO()` method.
	 * 
	 * @param indices 3D model's index data, which is used to bind the vertices of the
	 * model to the appropriate indices in the vertex array object (VAO).
	 * 
	 * 	- `indices`: An `int[]` array representing the indices of the vertices in the mesh.
	 * 	- `length`: The number of elements in the `indices` array, which is equal to the
	 * number of vertices in the mesh.
	 * 
	 * @returns a `Model` object representing a 3D model.
	 * 
	 * The returned Model object has an integer vao field that contains the Vulkan handle
	 * for the Vertex Array Object (VAO) created in the function.
	 * 
	 * The v_count field represents the number of vertices stored in the buffer.
	 * 
	 * The Model object is a wrapper around the Vulkan handle and provides an easy-to-use
	 * interface for manipulating the data.
	 */
	public static Model load(float[] vertices, int[] indices) {
		int vao = createVAO();
		storeIndices(indices);
		storeData(0, vertices);
		unbindVAO();
		int v_count = indices.length;
		return new Model(vao, v_count);
	}
	
	/**
	 * generates a new vertex array object (VBO) using the `glGenVertexArrays` method and
	 * binds it to the current context using `glBindVertexArray`.
	 * 
	 * @returns an integer value representing a valid vertex array object (VBO) handle.
	 */
	private static int createVAO() {
		int vao = GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vao);
		return vao;
	}
	
	/**
	 * creates a vertex buffer object (VBO) and binds it to a graphics processing unit
	 * (GPU). It then sets the data for two attributes using the `GLVertexAttribPointer`
	 * method and stores the buffer in memory.
	 * 
	 * @param attrib 3D vertex attribute to which the data is being written, with the
	 * first value representing the position of the vertex and the second value representing
	 * the texture coordinate of the vertex.
	 * 
	 * @param data 3D float array that contains the data to be stored in the vertex buffer
	 * object (VBO).
	 * 
	 * 	- The `data` array is of length 4, indicating that it contains 4 floating-point
	 * values.
	 * 	- The `attrib` parameter indicates the location of the vertex attribute in the
	 * GPU's vertex shader, which is used to store the data in the buffer. In this case,
	 * `attrib` is set to 0 and 1, indicating two vertex attributes.
	 * 	- The `GL_FLOAT` type specifies the data type of the values stored in the buffer.
	 * 	- The `false` value for the `bool` parameter indicates that the buffer data is
	 * not stored in a synchronized manner with respect to other buffers or the GPU's clock.
	 * 	- The `5 * 4` value for the first `GL_VERTEX_ATTRIB_Pointer` call indicates that
	 * each vertex attribute has 3 floating-point values, while the `3 * 4` value for the
	 * second call indicates that the second attribute has 2 floating-point values.
	 */
	private static void storeData(int attrib, float[] data) {
		int vbo = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, Util.createFlippedBuffer(data), GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attrib, 3, GL11.GL_FLOAT, false, 5 * 4,     0);
		GL20.glVertexAttribPointer(attrib + 1, 2, GL11.GL_FLOAT, false, 5 * 4, 3 * 4);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}
	
	/**
	 * generates a new buffer object using the `glGenBuffers` method, binds it with
	 * `glBindBuffer`, and then transfers the `indices` array data to the buffer using
	 * `glBufferData`. The buffer is created in a flipped format for use in an element
	 * array buffer.
	 * 
	 * @param indices 3D mesh's vertex positions, which are stored in a buffer object
	 * using the `GL_ELEMENT_ARRAY_BUFFER` buffer target and `GL_STATIC_DRAW` storage mode.
	 * 
	 * 	- `indices` is an instance of `int[]`.
	 * 	- The length of the array is determined by the serialization method used to create
	 * the buffer.
	 * 	- Each element in the array represents a 2D vertex index, which can range from 0
	 * to (width - 1) \* (height - 1).
	 * 	- The elements are stored in a contiguous block of memory, allowing for efficient
	 * access and manipulation.
	 */
	private static void storeIndices(int[] indices) {
		int ibo = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, ibo);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, Util.createFlippedBuffer(indices), GL15.GL_STATIC_DRAW);
	}
	
	/**
	 * terminates the binding of a Vertex Array Object (VAO) by passing the ID of the VAO
	 * to `GL30.glBindVertexArray`.
	 */
	private static void unbindVAO() {
		GL30.glBindVertexArray(0);
	}
	
}
