package com.ch;

import com.ch.math.Matrix4f;
import com.ch.math.Vector3f;

/**
 * is an abstract class that provides methods for calculating view and projection
 * matrices, as well as adjusting to a specified viewport size. It also has an abstract
 * method `getAsMatrix4()` which returns a Matrix4f object without providing any
 * implementation details. The class has several fields and methods for performing
 * image transformations, including scaling, rotation, and flipping.
 */
public abstract class Camera {

	protected Matrix4f projection;
	protected Matrix4f viewProjectionMat4;
	protected CameraStruct values;
	protected Transform transform;

	protected Camera(Matrix4f projection) {
		this.projection = projection;
		transform = new Transform();
	}

	/**
	 * computes and returns a `Matrix4f` object representing the view-projection
	 * transformation. If the `viewProjectionMat4` is null or has changed, it calculates
	 * the view matrix first.
	 * 
	 * @returns a Matrix4f object representing the view projection matrix.
	 * 
	 * 	- `viewProjectionMat4`: This is a 4x4 matrix that represents the view projection
	 * transformation. The rows represent the view and projection matrices, respectively.
	 * 	- `transform`: A flag indicating whether the view matrix has changed since the
	 * last call to `calculateViewMatrix()`. If it has, the view matrix is recomputed.
	 */
	public Matrix4f getViewProjection() {

		if (viewProjectionMat4 == null || transform.hasChanged()) {
			calculateViewMatrix();
		}

		return viewProjectionMat4;
	}

	/**
	 * calculates a view matrix for a 3D scene based on the rotation and translation of
	 * the camera, and applies the projection transformation to it.
	 * 
	 * @returns a matrix representing the view transformation of a camera.
	 * 
	 * 	- `viewProjectionMat4`: A 4x4 homogeneous matrix representing the view and
	 * projection transformations combined.
	 * 	- `cameraRotation`: A 3x3 rotation matrix representing the rotation of the camera
	 * around its center.
	 * 	- `cameraTranslation`: A 3x1 vector representing the translation of the camera
	 * relative to its original position.
	 * 
	 * The returned output is a product of two matrices: the projection matrix and the
	 * rotation matrix, followed by the translation vector. This structure allows for
	 * complex transformations such as rotations and translations of the view volume.
	 */
	public Matrix4f calculateViewMatrix() {

		Matrix4f cameraRotation = transform.getTransformedRot().conjugate().toRotationMatrix();
		Matrix4f cameraTranslation = getTranslationMatrix();

		return (viewProjectionMat4 = projection.mul(cameraRotation.mul(cameraTranslation)));

	}

	/**
	 * generates a matrix that represents a translation from the position of the transform's
	 * origin to a new position specified by a vector in screen space.
	 * 
	 * @returns a 4x4 matrix representing the translation of the camera position in 3D space.
	 * 
	 * 	- The output is a 4x4 matrix representing a translation in 3D space, with the x,
	 * y, and z components corresponding to the translation amount in each dimension.
	 * 	- The matrix is initialized using the `initTranslation` method, which takes three
	 * float arguments representing the x, y, and z coordinates of the translation.
	 * 	- The resulting matrix can be used for transformations such as moving an object
	 * from one location to another or applying a linear transformation to a 3D model.
	 */
	public Matrix4f getTranslationMatrix() {
		Vector3f cameraPos = transform.getTransformedPos().mul(-1);
		return new Matrix4f().initTranslation(cameraPos.getX(), cameraPos.getY(), cameraPos.getZ());
	}

	/**
	 * returns the `transform` object, which is an instance of the `Transform` class.
	 * 
	 * @returns a reference to an instance of the `Transform` class.
	 * 
	 * 	- The `transform` variable returns an instance of the `Transform` class, which
	 * represents a transformation in the context of image processing.
	 * 	- The `transform` variable is a field of the `ImageProcessing` class and can be
	 * accessed through the function call `getTransform()`.
	 * 	- The `Transform` class defines several methods for performing various image
	 * transformations, such as scaling, rotation, and flipping.
	 */
	public Transform getTransform() {
		return transform;
	}
	
	public abstract Matrix4f calculateProjectionMatrix(CameraStruct data);

	public abstract void adjustToViewport(int width, int height);

	/**
	 * is an abstract class that serves as a base class for other classes in the Camera
	 * package. It has an abstract method called `getAsMatrix4()` which returns a Matrix4f
	 * object, but does not provide any implementation details. The class does not contain
	 * any fields or methods beyond this one abstract method.
	 */
	protected abstract class CameraStruct {

		protected abstract Matrix4f getAsMatrix4();

	}

}
